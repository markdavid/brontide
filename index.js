const Discord = require("discord.js");
const config = require("./config.json");
const moment = require('moment');
const client = new Discord.Client();
const fs = require('fs')
const db = require('quick.db')
client.commands = new Discord.Collection()

fs.readdir('./cmds/', (err, files) => {

    if (err) console.log(err)

    let jsfile = files.filter(f => f.split(".").pop() === "js")
    if (jsfile.length <= 0) {
        return;
    }

    jsfile.forEach((f, i) => {
        let props = require(`./cmds/${f}`)
        client.commands.set(props.help.name, props)
    })
})

client.on("ready", async () => {
  console.log("           ")
  console.log("           ")
  console.log("           ")
  console.log("           ")
  console.log(`        • ${client.user.tag} •      `)
  console.log("       CODED Rhino      ")
  console.log("       copyright 2018, Brontide, MarkDavid Group, LLC     ")
  console.log('%c This is a success message!', 'color: green; font-weight: bold;');
  console.log('%c This is an error message!', 'color: red; font-weight: bold;');
  console.log("           ")
  console.log("           ")
  console.log("           ")



  client.user.setStatus(`idle`);
  client.user.setGame(`Brontide`,
   "https://www.twitch.tv/Mark")
});

const color = config.color;
const prefix = config.prefix;
const footer = config.footer;

client.on("message", async message => {

    if (message.author.bot) return;

    let messageArray = message.content.split(" ")
    let cmd = messageArray[0].toLowerCase()
    let args = messageArray.slice(1)

    if (!message.content.startsWith(prefix)) return;

    let commandfile = client.commands.get(cmd.slice(prefix.length))
    if (commandfile) commandfile.run(client, message, args, color, prefix)

    process.on('unhandledRejection', console.error);

});

client.on('messageReactionAdd', async (reaction, user) => {

if (reaction.count !== 1) {

let y = reaction.message;
let channel = reaction.message.channel;
let salesrep = channel.guild.roles.find(`name`, `Sales Rep`);
if (!salesrep);

 if (reaction.emoji.name === "2⃣" && reaction.message.channel.name.includes(`-ticket`)) {

 y.clearReactions();

 channel.overwritePermissions(salesrep, {
  READ_MESSAGES: true })

 let response = new Discord.RichEmbed()
 .setColor(color)
 .setAuthor(`${user.username}#${user.discriminator}`, `${user.avatarURL}`)
 .setDescription(`<:ragree:495954803273367573> | Our sales team have been added to this ticket! Thank you for your patience.`)
 .setFooter(footer)

 return channel.send({embed: response})

}

 }
})


client.login('TOKEN');

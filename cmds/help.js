const Discord = require ("discord.js");
const moment = require("moment")

const config = require("../config.json");

module.exports.run = async (client, message, args) => {
  
const color = config.color;
const footer = config.footer;
const prefix = config.prefix;
 
 let embed = new Discord.RichEmbed()
 .setColor(color)
 .setTitle("`📜` Commands")
 .setAuthor(client.user.username, client.user.displayAvatarURL)
 .setDescription(`• **${prefix}add** | Add a user to a ticket!\n• **${prefix}close** | Close an open ticket!\n• **${prefix}commission** | Post a commission
• **${prefix}finish** | Finish a discord setup!\n• **${prefix}globalannounce** | Global announce a message.\n• **${prefix}hr** | Create a human resource ticket!
• **${prefix}new** | Create a new ticket.\n• **${prefix}payment** | Create a payment request.\n• **${prefix}paypal** | Check a paypal.\n• **${prefix}portfolio** | Check a portfolio.
• **${prefix}remove** | Remove a user.\n• **${prefix}serverannounce** | Make a server announcement!\n• **${prefix}setpaypal** | Set your payal!
• **${prefix}setup** | Set up a discord!\n• **${prefix}supportdays** | Mark a ticket to be deleted.`)
 .setFooter(footer)
 
 message.channel.send({embed: embed})
  
}

module.exports.help = {
  name: "help" 
}
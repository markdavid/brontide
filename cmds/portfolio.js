const Discord = require ("discord.js");
const db = require("quick.db");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

let user = message.mentions.users.first() || message.author

  db.fetch(`portfolio_${message.author.id}`).then(q => {

   if (q === null) return message.channel.send(`<:rdisagree:495954827776360448> | **No Portfolio found for this user!**`)

   return message.channel.send(`**${user.username}'s Portfolio:** ${q}`)
  })

}

module.exports.help = {
  name: "portfolio"
}

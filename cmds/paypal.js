const Discord = require ("discord.js");
const db = require("quick.db");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

if (!message.member.roles.some(role => ["Freelancer"].includes(role.name)) && message.author.id !== '396026432792428545') return;

let user = message.mentions.users.first() || message.author

 db.fetch(`paypal_${user.id}`).then(q => {

   if (q === null) return message.channel.send(`<:rdisagree:495954827776360448> | **No PayPal has been found in the deep files!**`)

   return message.channel.send(`**${user.username}'s Paypal:** ${q}`)
 })

}

module.exports.help = {
  name: "paypal"
}

const Discord = require ("discord.js");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {
  
const color = config.color;
const footer = config.footer;
const prefix = config.prefix;
  
let error = new Discord.RichEmbed()
.setColor(color)
.setAuthor(message.author.tag, message.author.displayAvatarURL)
.setDescription("<:rdisagree:495954827776360448> | An `ERROR` occured.\n\n**Format:**\n`1.` **" + prefix + "commission** role | budget | deadline | description")
.setFooter(footer)

let posted = new Discord.RichEmbed()
.setColor(color)
.setAuthor(client.user.username, client.user.displayAvatarURL)
.setDescription(`Your commission has been posted. => Please be patient while we search for a freelancer that fits you!`)
.setFooter(footer)

 if (args.slice(0).join(` `) < 4) return message.channel.send({embed: error});
    
 const role = message.content.substring(12, message.content.indexOf(" | "));
 const budget = message.content.split(' | ')[1]
 const deadline = message.content.split(' | ')[2]
 const description = message.content.split(' | ')[3]  
  
 message.delete() 
 message.channel.send({embed: posted}).then(async function (post) {
   
  let commission = new Discord.RichEmbed()
  .setColor(color)
  .setThumbnail(message.author.avatarURL)
  .setTitle(`${client.user.username} • New Commission`)
  .setDescription(`Click the reaction below to claim this commission and be added to the ticket.`)
  .addField(`Ticket`, `${message.channel.name}`)
  .addField(`Role`, `${role}`)
  .addField(`Budget`, budget)
  .addField(`Deadline`, deadline)
  .addField(`Description`, description)
  .setFooter(footer)
   
 let channel = message.guild.channels.find("name", `commissions`)
 await channel.send(`${role}`).then(async function (msg1) {
 channel.send({embed: commission}).then(async function (msg2) {
      
 await msg2.react(`✅`);
       
 client.on('messageReactionAdd', async (reaction, user) => {
    
 if (reaction.count === 2) {

  let msg3 = reaction.message; 
  let ticket = reaction.message.channel;
  let guild = reaction.message.guild;
  
  if (reaction.emoji.name === "✅" && reaction.message.channel.name.includes(`commissions`)) {
    
  msg1.delete()
  msg3.clearReactions();
    
  message.channel.overwritePermissions(user, {
   EAD_MESSAGES: true })
    
    let reacted = new Discord.RichEmbed()
    .setColor(color)
    .setAuthor(`${user.tag}`, user.displayAvatarURL)
    .setDescription(`Your commission has been accepted. => Your wingman is ${user}!`)
    .setFooter(footer)
    
    post.edit({embed: reacted})
    
    let accepted = new Discord.RichEmbed()
    .setColor(color)
    .setThumbnail(user.displayAvatarURL)
    .setTitle(`${client.user.username} • Accepted`)
    .setDescription(`Commission claimed, continue here: **${message.channel.name}**`)
    .addField(`Ticket`, `${message.channel.name}`)
    .addField(`Role`, `${role}`)
    .addField(`Budget`, budget)
    .addField(`Deadline`, deadline)
    .addField(`Description`, description) 
    .addField(`Claimed By`, `${user}`)
    .setFooter(footer)
  
   msg3.edit({embed: accepted}) 
    
      }
     }
    })
   })
  })
 })
  
}

module.exports.help = {
  name: "commission" 
}
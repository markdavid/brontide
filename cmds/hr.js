const Discord = require("discord.js");
const db = require("quick.db");
const fs = require("fs");

const coolD = new Set();
const AcceptMessage = require("acceptmessage");
const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;
const ticket = message.channel;

  let cooldown = new Discord.RichEmbed()
  .setColor(color)
  .setAuthor(`${message.author.username}#${message.author.discriminator}`, `${message.author.avatarURL}`)
  .setDescription(`**You're now on lockdown!??!?!**\n\nCooldown Time: **10 Second(s)**`)

  if (coolD.has(message.author.id)) {
  message.channel.send(cooldown);

  } else {

   message.guild.createChannel(`${message.author.username}-hr`, 'text', [{
    id: message.guild.id,
    deny: ['READ_MESSAGES'],
    allow: ['SEND_MESSAGES']
   }]).then(channel => { channel.setParent(message.guild.channels.find("name", `HR`)).then(hrChannel => {

   hrChannel.overwritePermissions(message.author, {
    READ_MESSAGES: true })})
   channel.overwritePermissions(message.author, {
    READ_MESSAGES: true })

   let creationEmbed = new Discord.RichEmbed()
   .setColor(color)
   .setAuthor(`${message.author.username}#${message.author.discriminator}`, `${message.author.avatarURL}`)
   .setDescription(`<:ragree:495954803273367573> | Your HR ticket has been created!\n\n**Ticket ID:** #${channel.name}`)
   .setFooter(footer)

   message.channel.send({embed: creationEmbed})

   var hrEmbed = new Discord.RichEmbed()
   .setColor(color)
   .setAuthor(`${message.author.tag}`, `${message.author.avatarURL}`)
   .setDescription("<:ragree:495954803273367573> | **Thank you for creating a HR ticket!** Please be patient, someone will be with you shortly.")
   .setFooter(footer)

  return channel.send({embed: hrEmbed})

  })

  coolD.add(message.author.id);
   setTimeout(() => {
   coolD.delete(message.author.id);
   }, 10000);
  }

}

module.exports.help = {
  name: "hr"
}

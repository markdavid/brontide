const Discord = require("discord.js");
const db = require("quick.db");
const fs = require("fs");

const coolD = new Set();
const AcceptMessage = require("acceptmessage");
const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;
const ticket = message.channel;

  let cooldown = new Discord.RichEmbed()
  .setColor(color)
  .setAuthor(`${message.author.username}#${message.author.discriminator}`, `${message.author.avatarURL}`)
  .setDescription(`**You're now on lockdown!??!?!**\n\nCooldown Time: **10 Second(s)**`)

  if (coolD.has(message.author.id)) {
  message.channel.send(cooldown);

  } else {

let reactionEmbed = new Discord.RichEmbed()
.setColor(color)
.setAuthor(client.user.username, client.user.displayAvatarURL)
.setDescription(`**Ticket Creation**\nReact to this message to create a ticket.\nTickets are located under the Tickets category!`)
.setFooter(footer)

message.channel.send({embed: reactionEmbed}).then(async function (msg2) {
  await msg2.react(`✅`);
})

client.on('messageReactionAdd', async (reaction, user) => {

if (reaction.count === 2) {

  let msg3 = reaction.message;
  let ticket = reaction.message.channel;
  let guild = reaction.message.guild;

  if (reaction.emoji.name === "✅") {
  if (ticket.name === (`commissions`)) return;
    
   reaction.remove(user)

   message.guild.createChannel(`${message.author.username}-ticket`, 'text', [{
    id: message.guild.id,
    deny: ['READ_MESSAGES'],
    allow: ['SEND_MESSAGES']
   }]).then(channel => { channel.setParent(message.guild.channels.find("name", `Tickets`)).then(ticketChannel => {

   ticketChannel.overwritePermissions(message.author, {
    READ_MESSAGES: true })})
   channel.overwritePermissions(message.author, {
    READ_MESSAGES: true })

   var ticketEmbed = new Discord.RichEmbed()
   .setColor(color)
   .setAuthor(`${message.author.tag}`, `${message.author.avatarURL}`)
   .setDescription("Hello! Thank you for contacting MarkDavid Group, and welcome to the Wizard! To continue with your commission, continue by filling out the questions!")
   .setFooter(footer)

   channel.send({embed: ticketEmbed})

        let ticket = channel;

        let question1 = new Discord.RichEmbed()
        .setColor(color)
        .setAuthor(client.user.username, client.user.displayAvatarURL)
        .setFooter(footer)
        .setDescription(`Let's start off with the first question, which service do you require today?
         A: Website Development
         B: Discord Bot Development
         C: Video Editing
         D: Website Design
         E: System Administration
         F: Graphics Design
         G: Illustrations
         H: Trailer Creation`)

          return ticket.send({embed: question1}).then(async(t) => {

            ticket.awaitMessages(response => message.content,{
            time: 3600000,
            max: 1
            }).then((collect1) => {

            let zero = collect1.first().content
            if (zero) {

            let question2 = new Discord.RichEmbed()
            .setColor(color)
            .setFooter(footer)
            .setAuthor(client.user.username, client.user.displayAvatarURL)
            .setDescription(`Now, what is your budget for this project?`)

            return ticket.send({embed: question2}).catch(async(r) => {
              return message.channel.send(`time`)
        }).then(async(r) => {

            ticket.awaitMessages(response => message.content, {
            time: 3600000,
            max: 1
            }).then((collect2) => {

            let one = collect1.first().content
            if (one) {

            let question3 = new Discord.RichEmbed()
            .setColor(color)
            .setFooter(footer)
            .setAuthor(client.user.username, client.user.displayAvatarURL)
            .setDescription(`What is the description of the service you have in mind?`)

            return ticket.send({embed: question3}).catch(async(r) => {
              return message.channel.send(`time`)
            }).then(async(rrrr) => {

            ticket.awaitMessages(response => message.content,{
            time: 3600000,
            max: 1
            }).then((collect5) => {

            let four = collect2.first().content
            if (four) {

            let role = collect1.first().content
            let budget = collect2.first().content
            let description = collect5.first().content

            if (role === "A" || "a") {role = 'Website Development'}
            if (role === "B" || "b") {role = 'Discord Bot Development'}
            if (role === "C" || "c") {role = 'Video Editing'}
            if (role === "D" || "d") {role = 'Website Design'}
            if (role === "E" || "e") {role = 'System Administration'}
            if (role === "F" || "f") {role = 'Graphics Design'}
            if (role === "G" || "g") {role = 'Illustrations'}
            if (role === "H" || "h") {role = 'Trailer Creation'}

            if (!role) {role = 'Question Skipped.'}
            if (!budget) {budget = 'Question Skipped'}
            if (!description) {description = 'Question Skipped'}

            let commission = new Discord.RichEmbed()
            .setColor(color)
            .setThumbnail(message.author.avatarURL)
            .setTitle(`${client.user.username} • New Commission`)
            .setDescription(`Click the reaction below to claim this commission and be added to the ticket.`)
            .addField(`Ticket`, `#${ticket.name}`)
            .addField(`Role`, role)
            .addField(`Budget`, budget)
            .addField(`Description`, description)

            let t = message.guild.channels.find("name", `commissions`)
            t.send(`${role}`).then(async function (msg1) {
            await t.send({embed: commission}).then(async function (msg2) {
            await msg2.react(`✅`);

            let posted = new Discord.RichEmbed()
            .setColor(color)
            .setFooter(footer)
            .setAuthor(client.user.username, client.user.displayAvatarURL)
            .setDescription(`**Your commission has been posted.** • Please be patient while we search for a freelancer that fits you!`)

            return ticket.send({embed: posted}).then(async function(post) {

client.on('messageReactionAdd', async (reaction, user) => {

if (reaction.count === 2) {

  let msg3 = reaction.message;
  let ticket = reaction.message.channel;
  let guild = reaction.message.guild;

  if (reaction.emoji.name === "✅" && reaction.message.channel.name.includes(`commissions`)) {

  msg1.delete()
  msg3.clearReactions();

  message.channel.overwritePermissions(user, {
              READ_MESSAGES: true })

    let reacted = new Discord.RichEmbed()
    .setColor(color)
    .setAuthor(`${user.tag}`, user.displayAvatarURL)
    .setDescription(`Your commission has been accepted. • Your wingman is ${user}!`)

    post.edit({embed: reacted})

    let accepted = new Discord.RichEmbed()
    .setColor(color)
    .setThumbnail(user.displayAvatarURL)
    .setTitle(`${client.user.username} • Accepted`)
    .setDescription(`Commission claimed, continue here: **${message.channel.name}**`)
    .addField(`Ticket`, `${message.channel.name}`)
    .addField(`Sales Rep`, `${message.author}`)
    .addField(`Role`, `${role}`)
    .addField(`Budget`, budget)
    .addField(`Description`, description)
    .addField(`Claimed By`, `${user}`)

  msg3.edit({embed: accepted})

   }
   }

   }).catch(async() => {
    return; });

   })
   })
   })
   }
   })
   })
   }
   })
   })
   }
   })

    })
  })
}
}
})

  coolD.add(message.author.id);
   setTimeout(() => {
   coolD.delete(message.author.id);
   }, 10000);
  }

}

module.exports.help = {
  name: "sendembedreact"
}

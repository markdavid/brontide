const Discord = require ("discord.js");
const db = require("quick.db");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

let errorEmbed = new Discord.RichEmbed()
.setColor(color)
.setAuthor(message.author.tag, message.author.displayAvatarURL)
.setDescription("<:rdisagree:495954827776360448> | An `ERROR` occured.\n\n**Format:**\n`1.` **" + prefix + "setportfolio** <Your Portfolio Link>")
.setFooter(footer)

if (!message.member.roles.some(role => ["Freelancer"].includes(role.name)) && message.author.id !== '396026432792428545') return;

let content = message.content.split(' ').slice(1).join(' ');
if (!content) return message.channel.send({embed: errorEmbed})

  db.set(`portfolio_${message.author.id}`, content).then(message.channel.send(`<:ragree:495954803273367573> | **Portfolio Set!**`))

}

module.exports.help = {
  name: "setportfolio"
}

const Discord = require("discord.js");
const db = require("quick.db");
const ms = require("ms");
const fs = require("fs");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

if (!message.member.roles.some(role => ["Management"].includes(role.name)) && message.author.id !== '396026432792428545') return;

message.channel.send(`<:ragree:495954803273367573> | **This channel will be removed in 30 Days!**`)

  setTimeout(function(){
     message.channel.delete()
   }, 259200000);

}

module.exports.help = {
  name:"supportdays"
}

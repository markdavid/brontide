const Discord = require ("discord.js");
const db = require("quick.db");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

let error = new Discord.RichEmbed()
.setColor(color)
.setAuthor(message.author.tag, message.author.displayAvatarURL)
.setDescription("<:rdisagree:495954827776360448> | An `ERROR` occured.\n\n**Format:**\n`1.` **" + prefix + "payment** channel | payment")
.setFooter(footer)

if (!message.member.roles.some(role => ["Freelancer"].includes(role.name)) && message.author.id !== '396026432792428545') return;

if (args.slice(0).join(` `) < 4) return message.channel.send({embed: error});

const channel = message.content.substring(8, message.content.indexOf(" | "));
const amount = message.content.split(' | ')[1]

 db.fetch(`paypal_${message.author.id}`).then(q => {

  if (q === null) { q === "User does not have a PayPal set."}

let embed = new Discord.RichEmbed()
.setColor(color)
.setAuthor(client.user.username, client.user.displayAvatarURL)
.setFooter(footer)
.addField(`Ticket`, channel)
.addField(`Amount`, amount)
.addField(`PayPal`, q)

let log = message.guild.channels.find("name", `payments`)

message.channel.send(`<:ragree:495954803273367573> | **Payment awaiting approval!**`)
return log.send({embed: embed})

 })

}

module.exports.help = {
  name: "payment"
}

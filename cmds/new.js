const Discord = require("discord.js");
const db = require("quick.db");
const fs = require("fs");

const coolD = new Set();
const AcceptMessage = require("acceptmessage");
const config = require("../config.json");

module.exports.run = async (client, message, args) => {

// const color = "#FFFFFF";
// const footer = "just a test";
// const prefix = "r!";
const color = config.color;
const footer = config.footer;
const prefix = config.prefix;
const ticket = message.channel;

  let cooldown = new Discord.RichEmbed()
  .setColor(color)
  .setAuthor(`${message.author.username}#${message.author.discriminator}`, `${message.author.avatarURL}`)
  .setDescription(`**You're now on lockdown!??!?!**\n\nCooldown Time: **10 Second(s)**`)

  if (coolD.has(message.author.id)) {
  message.channel.send(cooldown);

  } else {

   message.guild.createChannel(`${message.author.username}-ticket`, 'text', [{
    id: message.guild.id,
    deny: ['READ_MESSAGES'],
    allow: ['SEND_MESSAGES']
   }]).then(channel => { channel.setParent(message.guild.channels.find("name", `Tickets`)).then(ticketChannel => {

   ticketChannel.overwritePermissions(message.author, {
    READ_MESSAGES: true })})
   channel.overwritePermissions(message.author, {
    READ_MESSAGES: true })

   let creationEmbed = new Discord.RichEmbed()
   .setColor(color)
   .setAuthor(`${message.author.username}#${message.author.discriminator}`, `${message.author.avatarURL}`)
   .setDescription(`<:ragree:495954803273367573> | Your ticket has been created!\n\n**Ticket ID:** #${channel.name}`)
   .setFooter(footer)

   message.channel.send({embed: creationEmbed})

   var ticketEmbed = new AcceptMessage(client, {
   content: new Discord.RichEmbed()
   .setColor(color)
   .setAuthor(`${message.author.tag}`, `${message.author.avatarURL}`)
   .setDescription("Hello! Thank you for contacting us.\nReact with :one: to start the wizard.\nReact with :two: to speak to a sales representive.")
   .setFooter(footer),
        emotes: {
            accept: '1⃣',
            deny: '2⃣'
        },
        checkUser: message.author,
        actions: {
        deny: (reaction, user) => {

        },

        accept: (reaction, user) => {

        let ticket = channel;

        let question1 = new Discord.RichEmbed()
        .setColor(color)
        .setAuthor(client.user.username, client.user.displayAvatarURL)
        .setFooter(footer)
        .setDescription(`Let's start with the first question. Which service do you require today?
         A: Discord Setups
		 B: Server Setups
		 C: Configuration
		 D: Spigot Developer
		 E: Sponge/Forge Developer
		 F: Frontend Developer
		 G: Backend Developer
		 H: Xenforo
		 I: Buycraft/MCM
		 J: Builder
		 K: Terraformer
		 L: GFX
		 M: Writer
		 N: Render Artist
		 O: UI/UX
		 P: Trailer Creation
		 Q: Video Editor
		 R: Illustrator
		 S: System Admin`)

          return ticket.send({embed: question1}).then(reaction.message.clearReactions()).then(async(t) => {

            ticket.awaitMessages(response => message.content,{
            time: 3600000,
            max: 1
            }).then((collect1) => {

            let zero = collect1.first().content
            if (zero) {

            let question2 = new Discord.RichEmbed()
            .setColor(color)
            .setFooter(footer)
            .setAuthor(client.user.username, client.user.displayAvatarURL)
            .setDescription(`Now, what is your budget for this project?`)

            return ticket.send({embed: question2}).catch(async(r) => {
              return message.channel.send(`time`)
        }).then(async(r) => {

            ticket.awaitMessages(response => message.content, {
            time: 3600000,
            max: 1
            }).then((collect2) => {

            let one = collect1.first().content
            if (one) {

            let question3 = new Discord.RichEmbed()
            .setColor(color)
            .setFooter(footer)
            .setAuthor(client.user.username, client.user.displayAvatarURL)
            .setDescription(`What is the description of the service you have in mind?`)

            return ticket.send({embed: question3}).catch(async(r) => {
              return message.channel.send(`time`)
            }).then(async(rrrr) => {

            ticket.awaitMessages(response => message.content,{
            time: 3600000,
            max: 1
            }).then((collect5) => {

            let four = collect2.first().content
            if (four) {

            let role = zero
            let budget = collect2.first().content
            let description = collect5.first().content

            if (role === "A" || role === "a") {role = 'Discord Setups'}
            if (role === "B" || role === "b") {role = 'Server Setups'}
            if (role === "C" || role === "c") {role = 'Configuration'}
            if (role === "D" || role === "d") {role = 'Spigot Developer'}
            if (role === "E" || role === "e") {role = 'Sponge/Forge Developer'}
            if (role === "F" || role === "f") {role = 'Frontend Developer'}
            if (role === "G" || role === "g") {role = 'Backend Developer'}
            if (role === "H" || role === "h") {role = 'Xenforo'}
			if (role === "I" || role === "i") {role = 'Buycraft/MCM'}
			if (role === "J" || role === "j") {role = 'Builder'}
			if (role === "K" || role === "k") {role = 'Terraformer'}
			if (role === "L" || role === "l") {role = 'GFX'}
			if (role === "M" || role === "m") {role = 'Writer'}
			if (role === "N" || role === "N") {role = 'Render Artist'}
			if (role === "O" || role === "o") {role = 'UI/UX'}
			if (role === "P" || role === "p") {role = 'Trailer Creation'}
			if (role === "Q" || role === "q") {role = 'Video Editor'}
			if (role === "R" || role === "r") {role = 'Illustrator'}
			if (role === "S" || role === "s") {role = 'System Admin'}

            if (!role) {role = 'Question Skipped.'}
            if (!budget) {budget = 'Question Skipped'}
            if (!description) {description = 'Question Skipped'}

            let commission = new Discord.RichEmbed()
            .setColor(color)
            .setThumbnail(message.author.avatarURL)
            .setTitle(`${client.user.username} • New Commission`)
            .setDescription(`Click the reaction below to claim this commission and be added to the ticket.`)
            .addField(`Ticket`, `#${ticket.name}`)
            .addField(`Role`, role)
            .addField(`Budget`, budget)
            .addField(`Description`, description)

            let t = message.guild.channels.find("name", `commissions`)

            let guildRole = message.guild.roles.find(`name`, role);

            if (guildRole) role = guildRole
              else role = role

            t.send(`${role}`).then(async function (msg1) {
            await t.send({embed: commission}).then(async function (msg2) {
            await msg2.react(`✅`);

            let posted = new Discord.RichEmbed()
            .setColor(color)
            .setFooter(footer)
            .setAuthor(client.user.username, client.user.displayAvatarURL)
            .setDescription(`**Your commission has been posted.** • Please be patient while we search for a freelancer that fits you!`)

            return ticket.send({embed: posted}).then(async function(post) {

client.on('messageReactionAdd', async (reaction, user) => {

if (reaction.count === 2) {

  let msg3 = reaction.message;
  let ticket = reaction.message.channel;
  let guild = reaction.message.guild;

  if (reaction.emoji.name === "✅" && reaction.message.channel.name.includes(`commissions`)) {

  msg1.delete()
  msg3.clearReactions();

  message.channel.overwritePermissions(user, {
              READ_MESSAGES: true })

    let reacted = new Discord.RichEmbed()
    .setColor(color)
    .setAuthor(`${user.tag}`, user.displayAvatarURL)
    .setDescription(`Your commission has been accepted. • Your wingman is ${user}!`)

    post.edit({embed: reacted})

    let accepted = new Discord.RichEmbed()
    .setColor(color)
    .setThumbnail(user.displayAvatarURL)
    .setTitle(`${client.user.username} • Accepted`)
    .setDescription(`Commission claimed, continue here: **${message.channel.name}**`)
    .addField(`Ticket`, `${message.channel.name}`)
    .addField(`Sales Rep`, `${message.author}`)
    .addField(`Role`, `${role}`)
    .addField(`Budget`, budget)
    .addField(`Description`, description)
    .addField(`Claimed By`, `${user}`)

  msg3.edit({embed: accepted})

   }
   }
   })
   })

   }).catch(async() => {
    return; });

   })
   }
   })
   })
   }
   })
   })
   }
   })
   })
   }
   }
   })

   ticketEmbed.send(channel) })

  coolD.add(message.author.id);
   setTimeout(() => {
   coolD.delete(message.author.id);
   }, 10000);
  }

}

module.exports.help = {
  name: "new"
}

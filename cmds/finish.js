const Discord = require("discord.js");
const db = require("quick.db");
const fs = require("fs");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

if (!message.member.roles.some(role => ["Management"].includes(role.name)) && message.author.id !== '396026432792428545') return;

      const categoryG = message.guild.channels.find(c => c.name === 'general' && c.type === 'category');

      // Administration Category

      const categoryAD = message.guild.channels.find(c => c.name === 'administration' && c.type === 'category');

      // Staff Category

      const categoryS = message.guild.channels.find(c => c.name === 'staff' && c.type === 'category');

      // Executives Category

      const categoryE = message.guild.channels.find(c => c.name === 'executives' && c.type === 'category');

    // Channel Checks

    const cG = message.guild.channels.find(c => c.name === 'general' && c.type === 'channel'); // General

    if (!cG) {

      await message.guild.createChannel('general')
      .then(channel => channel.setParent(categoryG))
      .catch(console.error);

    }

    const cOT = message.guild.channels.find(c => c.name === 'off-topic' && c.type === 'channel'); // Off-Topic

    if (!cOT) {

      await message.guild.createChannel('off-topic')
      .then(channel => channel.setParent(categoryG))
      .catch(console.error);

    }

    const cSC = message.guild.channels.find(c => c.name === 'showcase' && c.type === 'channel'); // Showcase

    if (!cSC) {

      await message.guild.createChannel('showcase')
      .then(channel => channel.setParent(categoryG))
      .catch(console.error);

    }

    const cA = message.guild.channels.find(c => c.name === 'announcements' && c.type === 'channel'); // Announcements

    if (!cA) {

      await message.guild.createChannel('announcements')
      .then(channel => channel.setParent(categoryAD))
      .catch(console.error);

    }

    const cTOS = message.guild.channels.find(c => c.name === 'tos' && c.type === 'channel'); // TOS

    if (!cTOS) {

      await message.guild.createChannel('tos')
      .then(channel => channel.setParent(categoryAD))
      .catch(console.error);

    }


    const cI = message.guild.channels.find(c => c.name === 'info' && c.type === 'channel'); // Info

    if (!cI) {

      await message.guild.createChannel('info')
      .then(channel => channel.setParent(categoryAD))
      .catch(console.error);

    }


    const cC = message.guild.channels.find(c => c.name === 'commissions' && c.type === 'channel'); // Commissions

    if (!cC) {

      await message.guild.createChannel('commissions')
      .then(channel => channel.setParent(categoryS))
      .catch(console.error);

    }


    const cS = message.guild.channels.find(c => c.name === 'staff' && c.type === 'channel'); // Staff

    if (!cS) {

      await message.guild.createChannel('staff')
      .then(channel => channel.setParent(categoryS))
      .catch(console.error);

    }

    if (!cS) {

      await message.guild.createChannel('staff-announcements')
      .then(channel => channel.setParent(categoryS))
      .catch(console.error);

    }

    if (!cS) {

      await message.guild.createChannel('bots')
      .then(channel => channel.setParent(categoryS))
      .catch(console.error);

    }

    const cCC = message.guild.channels.find(c => c.name === 'commission-chat' && c.type === 'channel'); // Commission-Chat

    if (!cCC) {

      await message.guild.createChannel('commission-chat')
      .then(channel => channel.setParent(categoryS))
      .catch(console.error);

    }

    const cEC = message.guild.channels.find(c => c.name === 'chat' && c.type === 'channel'); // Executive Chat

    if (!cEC) {

      await message.guild.createChannel('chat')
      .then(channel => channel.setParent(categoryE))
      .catch(console.error);

    }

    if (!cEC) {

      await message.guild.createChannel('payments')
      .then(channel => channel.setParent(categoryE))
      .catch(console.error);

    }

    const cEL = message.guild.channels.find(c => c.name === 'logs' && c.type === 'channel'); // Logs

    if (!cEL) {

      await message.guild.createChannel('logs')
      .then(channel => channel.setParent(categoryE))
      .catch(console.error);

    }

      const embed = new Discord.RichEmbed()
      .setTitle(`:white_check_mark: **Configuration Complete**`)
      .setColor(config.color);

      await message.channel.send(embed);

}

module.exports.help = {
  name:"finish"
}

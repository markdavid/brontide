const Discord = require ("discord.js");
const moment = require("moment")

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

 let errorEmbed = new Discord.RichEmbed()
 .setColor(color)
 .setAuthor(message.author.tag, message.author.avatarURL)
 .setDescription("<:rdisagree:495954827776360448> | An `ERROR` has occured.\n\n`1.` Make sure you provide a user's @, or id!\n`2.` Make sure this is a ticket channel!")
 .setFooter(footer)

  if (!message.channel.name.includes('-ticket')) return message.channel.send({embed: errorEmbed});

  let user = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
  if (!user) return message.channel.send({embed: errorEmbed})

 let endEmbed = new Discord.RichEmbed()
 .setColor(color)
 .setDescription(`<:ragree:495954803273367573> | ${user} has been removed from ticket!`)
 .setTimestamp()
 .setFooter(footer)
 .setAuthor(user.user.tag, user.user.displayAvatarURL)

  message.channel.overwritePermissions(user, {
    READ_MESSAGES: false });

  message.delete();
  return message.channel.send({embed: endEmbed});

}

module.exports.help = {
  name: "remove"
}

const Discord = require("discord.js");
const db = require("quick.db");
const fs = require("fs");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {
  
const color = config.color;
const footer = config.footer;
const prefix = config.prefix;
  
 let confirm = new Discord.RichEmbed()
 .setColor(color)
 .setAuthor(message.author.tag, message.author.displayAvatarURL)
 .setDescription("- **This commission will be closed in** • 10s\n\nType `cancel` to cancel the close action.")
 .setFooter(footer)

 let cancelEmbed = new Discord.RichEmbed()
 .setColor(color)
 .setAuthor(message.author.tag, message.author.displayAvatarURL)
 .setDescription(`<:ragree:495954803273367573> | Commission close status: **Canceled**`)
 .setFooter(footer)
 
if (!message.channel.name.includes('-ticket')) return;

message.channel.send({embed: confirm}).then(async(t) => {

   message.channel.awaitMessages(response => message.content, {
    max: 1,
    time: 10000,
   }).then((collected) => {
            
   if (collected.first().content === "cancel") {
            
    return t.edit({embed: cancelEmbed}) } 
            
    }).catch(async() => {
    message.channel.delete() })
  })
  
}

module.exports.help = {
  name:"close"
}

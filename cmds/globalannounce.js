const Discord = require ("discord.js");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

if (!message.member.roles.some(role => ["Management"].includes(role.name)) && message.author.id !== '396026432792428545') return;

let content = message.content.split(' ').slice(1).join(' ');

 let errorEmbed = new Discord.RichEmbed()
 .setColor(color)
 .setAuthor(message.author.tag, message.author.avatarURL)
 .setDescription("<:rdisagree:495954827776360448> | An `ERROR` has occured.\n\n`1.` State what you want to announce!")
 .setFooter(footer)

 let endEmbed = new Discord.RichEmbed()
 .setColor(color)
 .setAuthor(client.user.username, client.user.displayAvatarURL)
 .setDescription(content)
 .setFooter(footer)

let announce = client.channels.find("name", `announcements`)
if (!content) return message.channel.send({embed: errorEmbed})

return announce.send({embed: endEmbed}).then(message.channel.send(`<:ragree:495954803273367573> | **Global Announcement Made!**`))

}

module.exports.help = {
  name: "globalannounce"
}

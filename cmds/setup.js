const Discord = require("discord.js");
const db = require("quick.db");
const fs = require("fs");

const config = require("../config.json");

module.exports.run = async (client, message, args) => {

const color = config.color;
const footer = config.footer;
const prefix = config.prefix;

if (!message.member.roles.some(role => ["Management"].includes(role.name)) && message.author.id !== '396026432792428545') return;

      // Categories

      const categoryG = message.guild.channels.find(c => c.name === 'general' && c.type === 'category');

      const categoryAD = message.guild.channels.find(c => c.name === 'administration' && c.type === 'category');

      const categoryS = message.guild.channels.find(c => c.name === 'staff' && c.type === 'category');

      const categoryE = message.guild.channels.find(c => c.name === 'executives' && c.type === 'category');

      const categoryFF = message.guild.channels.find(c => c.name === 'Tickets' && c.type === 'category');

      const categoryF = message.guild.channels.find(c => c.name === 'HR' && c.type === 'category');

      // Roles - (Fucking Hell)

      let pres = message.guild.roles.find(r => r.name === "President"); // President

      let vpres = message.guild.roles.find(r => r.name === "Vice President"); // Vice President

      let mdt = message.guild.roles.find(r => r.name === "markDavid Team"); // markDavid Team

      let mm = message.guild.roles.find(r => r.name === "Management"); // Management

      let sr = message.guild.roles.find(r => r.name === "Sales Representative"); // Sales Representative

      let bots = message.guild.roles.find(r => r.name === "Bots"); // Bots

      let su = message.guild.roles.find(r => r.name === "Setups"); // Setups

      let dsu = message.guild.roles.find(r => r.name === "Discord Setups"); // Discord Setups

      let ssu = message.guild.roles.find(r => r.name === "Server Setups"); // Server Setups

      let cfi = message.guild.roles.find(r => r.name === "Configuration"); // Configuration

      let dev = message.guild.roles.find(r => r.name === "Developer"); // Developer

      let bdev = message.guild.roles.find(r => r.name === "Bot Developer"); // Bot Developer

      let sdev = message.guild.roles.find(r => r.name === "Spigot Developer"); // Spigot Developer

      let sfdev = message.guild.roles.find(r => r.name === "Sponge/Forge Developer"); // Sponge/Forge Developer

      let fed = message.guild.roles.find(r => r.name === "Frontend Developer"); // Frontend Developer

      let bed = message.guild.roles.find(r => r.name === "Backend Developer"); // Backend Developer

      let xen = message.guild.roles.find(r => r.name === "Xenforo"); // Xenforo

      let buyc = message.guild.roles.find(r => r.name === "Buycraft/MCM"); // Buycraft/MCM

      let buid = message.guild.roles.find(r => r.name === "Building"); // Building

      let buidr = message.guild.roles.find(r => r.name === "Builder"); // Builder

      let terr = message.guild.roles.find(r => r.name === "Terraformer"); // Terraformer

      let dzn = message.guild.roles.find(r => r.name === "Design"); // Design

      let wrt = message.guild.roles.find(r => r.name === "Writer"); // Writer

      let gfx = message.guild.roles.find(r => r.name === "GFX"); // GFX

      let ra = message.guild.roles.find(r => r.name === "Render Artist"); // Render Artist

      let va = message.guild.roles.find(r => r.name === "Vector Artist"); // Vector Artist

      let da = message.guild.roles.find(r => r.name === "Drawn Artist"); // Drawn Artist

      let uiux = message.guild.roles.find(r => r.name === "UI/UX"); // UI/UX

      let tcr = message.guild.roles.find(r => r.name === "Trailer Creator"); // Trailer Creator

      let ve = message.guild.roles.find(r => r.name === "Video Editor"); // Video Editor

      let il = message.guild.roles.find(r => r.name === "Illustrator"); // Illustrator

      let sysa = message.guild.roles.find(r => r.name === "System Admin"); // System Admin

      let lin = message.guild.roles.find(r => r.name === "Linux"); // Linux

      let ubu = message.guild.roles.find(r => r.name === "Ubuntu"); // Ubuntu

      let win = message.guild.roles.find(r => r.name === "Windows"); // Windows

      let frl = message.guild.roles.find(r => r.name === "Freelancer"); // Freelancer

      let pn = message.guild.roles.find(r => r.name === "Partner"); // Partner

      let rs = message.guild.roles.find(r => r.name === "Retired Staff"); // Retired Staff

      let aff = message.guild.roles.find(r => r.name === "Affiliate"); // Affiliate

      let mem = message.guild.roles.find(r => r.name === "Member"); // Member

      let cli = message.guild.roles.find(r => r.name === "Client"); // Client


      // Is Setup?

      if(categoryG && categoryAD && categoryS && categoryE) {
        message.delete();
        message.channel.send(new Discord.RichEmbed().setColor("#FF9090").setTitle(`**The server has already been configured**`))
        .then(msg => {
          msg.delete(5000)
        })
        .catch();
        return;
      }


      // General Category

      if (!categoryG) {

      await message.guild.createChannel('general', 'category', [{
        id: message.guild.id,
        allow: ['SEND_MESSAGES', 'READ_MESSAGES']
      }])
      .then(c => c.setPosition(0))
      .catch(console.error);
    }

      // Administration Category

      if (!categoryAD) {

      await message.guild.createChannel('administration', 'category', [{
        id: message.guild.id,
        deny: ['SEND_MESSAGES', 'READ_MESSAGES']
      }])
      .then(c => c.setPosition(1))
      .catch(console.error);
    }

      // Staff Category

      if (!categoryS) {

      await message.guild.createChannel('staff', 'category', [{
        id: message.guild.id,
        deny: ['SEND_MESSAGES', 'READ_MESSAGES']
      }])
      .then(c => c.setPosition(2))
      .catch(console.error);
    }

      // Executives Category

      if (!categoryE) {

      await message.guild.createChannel('executives', 'category', [{
        id: message.guild.id,
        deny: ['SEND_MESSAGES', 'READ_MESSAGES']
      }])
      .then(c => c.setPosition(3))
      .catch(console.error);
    }

	  if (!categoryFF) {

      await message.guild.createChannel('tickets', 'category', [{
        id: message.guild.id,
        deny: ['SEND_MESSAGES', 'READ_MESSAGES']
      }])
      .then(c => c.setPosition(3))
      .catch(console.error);
    }

	  if (!categoryF) {

      await message.guild.createChannel('hr', 'category', [{
        id: message.guild.id,
        deny: ['SEND_MESSAGES', 'READ_MESSAGES']
      }])
      .then(c => c.setPosition(3))
      .catch(console.error);
    }

    // President

    if(!pres) {
      pres = await message.guild.createRole({
        name: "President",
        color: "#4286F4",
        permissions: ['ADMINISTRATOR']
      })
    }

    // Vice President

    if(!vpres) {
      vpres = await message.guild.createRole({
        name: "Vice President",
        color: "#84B3FF",
        permissions: ['ADMINISTRATOR']
      })
    }

    // markDavid Team

    if(!mdt) {
      mdt = await message.guild.createRole({
        name: "markDavid Team",
        color: "#FF9244"
      })
    }

    // Management

    if(!mm) {
      mm = await message.guild.createRole({
        name: "Management",
        color: "#FFC344"
      })
    }

    // Sales Representative

    if(!sr) {
      sr = await message.guild.createRole({
        name: "Sales Representative",
        color: "#77ED44"
      })
    }

    // Bots

    if(!bots) {
      bots = await message.guild.createRole({
        name: "Bots",
        color: "#FF9244",
        permissions: ['ADMINISTRATOR']
      })
    }

    // Setups

    if(!su) {
      su = await message.guild.createRole({
        name: "Discord Setups",
        color: "#616b7a"
      })
    }

    // Discord Setups

    if(!dsu) {
      dsu = await message.guild.createRole({
        name: "Sever Setups",
        color: "#616b7a"
      })
    }

    // Server Setups

    if(!ssu) {
      ssu = await message.guild.createRole({
        name: "Configuration",
        color: "#616b7a"
      })
    }

    // Configuration

    if(!cfi) {
      cfi = await message.guild.createRole({
        name: "Spigot Developer",
        color: "#616b7a"
      })
    }

    // Developer

    if(!dev) {
      dev = await message.guild.createRole({
        name: "Sponge/Forge Developer",
        color: "#616b7a"
      })
    }

    // Bot Developer

    if(!bdev) {
      bdev = await message.guild.createRole({
        name: "Frontend Developer",
        color: "#616b7a"
      })
    }

    // Spigot Developer

    if(!sdev) {
      sdev = await message.guild.createRole({
        name: "Backend Developer",
        color: "#616b7a"
      })
    }

    // Sponge/Forge Developer

    if(!sfdev) {
      sfdev = await message.guild.createRole({
        name: "Xenforo",
        color: "#616b7a"
      })
    }

    // Frontend Developer

    if(!fed) {
      fed = await message.guild.createRole({
        name: "Buycraft/MCM",
        color: "#616b7a"
      })
    }

    // Backend Developer

    if(!bed) {
      bed = await message.guild.createRole({
        name: "Builder",
        color: "#616b7a"
      })
    }

    // Xenforo

    if(!xen) {
      xen = await message.guild.createRole({
        name: "Terraformer",
        color: "#616b7a"
      })
    }

    // Buycraft/MCM

    if(!buyc) {
      buyc = await message.guild.createRole({
        name: "GFX",
        color: "#616b7a"
      })
    }

    // Building

    if(!buid) {
      buid = await message.guild.createRole({
        name: "Writer",
        color: "#616b7a"
      })
    }

    // Builder

    if(!buidr) {
      buidr = await message.guild.createRole({
        name: "Render Artist",
        color: "#616b7a"
      })
    }

    // Terraformer

    if(!terr) {
      terr = await message.guild.createRole({
        name: "UI/UX",
        color: "#616b7a"
      })
    }

    // Design

    if(!dzn) {
      dzn = await message.guild.createRole({
        name: "Trailer Creator",
        color: "#616b7a"
      })
    }

    // Writer

    if(!wrt) {
      wrt = await message.guild.createRole({
        name: "Video Editor",
        color: "#616b7a"
      })
    }

    // GFX

    if(!gfx) {
      gfx = await message.guild.createRole({
        name: "Illustrator",
        color: "#616b7a"
      })
    }

    // Render Artist

    if(!ra) {
      ra = await message.guild.createRole({
        name: "System Admin",
        color: "#616b7a"
      })
    }

    // Freelancer

    if(!frl) {
      frl = await message.guild.createRole({
        name: "Freelancer",
        color: "#FF877A"
      })
    }

    // Partner

    if(!pn) {
      pn = await message.guild.createRole({
        name: "Partner",
        color: "#8A83C6"
      })
    }

    // Retired Staff

    if(!rs) {
      rs = await message.guild.createRole({
        name: "Retired Staff",
        color: "#BCA000"
      })
    }

    // Affiliate

    if(!aff) {
      aff = await message.guild.createRole({
        name: "Affiliate",
        color: "#8ADBD7"
      })
    }

    // Member

    if(!mem) {
      mem = await message.guild.createRole({
        name: "Member",
        color: "#FFC672"
      })
    }

    // Client

    if(!cli) {
      cli = await message.guild.createRole({
        name: "Client",
        color: "#E88940"
      })
    }

      const embed = new Discord.RichEmbed()
      .setTitle('**Use `-finish` to complete the configuration**')
      .setColor(config.color);

      await message.channel.send(embed);

}

module.exports.help = {
  name:"setup"
}
